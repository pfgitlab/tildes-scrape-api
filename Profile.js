// For parsing profile, the shared comment parser can be used along with a seperate parser for titles
const cheerio = require("cheerio");

let order = ["activity", "newest", "comments", "votes"];
let period = ["1h", "12h", "24h", "3d", "all", "xh/xd"]; // xh || xd || all

function recursiveParseComments($, parent) {
	let comments = parent.children("article");
	let res = [];

	comments.each((i, jqib) => {
		let article = $(jqib);

		let commentData = {};

		let comment = article.children(".comment-itself");
		let header = comment.find("header");
		let text = comment.find(".comment-text");
		let time = header.find(".time-responsive"); // Search in the header just in case users are ever allowed to put times in their comments

		// TODO user pages = author, new userpage()
		// TODO if post buttons contains reply, have reply be a function taking a string and replying it. Waiting for ajax for that.
		// FutureTODO I wish if the site ever saves your collapses add a collapse function to send a collapse message

		let replies = article.find(".comment-replies");
		if(replies.length) {
			commentData.replies = recursiveParseComments($, replies);
		}

		commentData.time = {
			staticDescription: time.find(".time-responsive-full").text(),
			time: new Date(time.attr("datetime"))
		};

		commentData.text = { // TODO temporary disable so I can see the console output
			unformatted: text.text(),
			html: text.html(),
			markdown: new Error("Not available until the api is") // TODO or some kind of html to markdown parser?
		};

		res.push(commentData);
	});

	return res;
}


class Profile { // TODO get around the issue of circular dependencies - how can page ask for comments and comments also ask for page?
	constructor(tildes, group, id, sorting = {}) { // TODO support links to specific comments
		this.tildes = tildes;
		this.group = group;
		this.id = id;
		this.sorting = sorting;

		this.data = {};
	}
	sort(orderperiod) {
		this.sorting = Object.assign(this.sorting, orderperiod);
	}
	get url() { // https://tildes.net/~tildes/4cd
		return `https://tildes.net/${
			encodeURIComponent(this.group)
		}/${
			encodeURIComponent(this.id)
		}?${
			Object.keys(this.sorting).map(key =>
				`${encodeURIComponent(key)}=${encodeURIComponent(this.sorting[key])}`
			).join`&`
		}`;
	}
	async load(fake) {
		let url = this.url;
		let data = fake ? {body: fake} : await this.tildes.get(this.url);
		// Now for the scraping
		const $ = cheerio.load(data.body);
		// Loop over each article. Each article contains a post
		// Articles contain a header title and a .topic-metadata group and a details description
		let res = {
			post: { // this should contain the same info as post from page.js rip

			}, comments: recursiveParseComments($, $(".topic-comments"))
		};

		let header = $(".topic-full").children("header"); // Can't just serch header because comments have them
		let voting = $(".topic-voting");

		res.post.title = header.find("h1").text();
		res.post.votes = {
			votes: voting.find(".topic-voting-votes").text(),
			dataICPutTo: voting.attr("data-ic-put-to")
		};

		// First, let's get info about the post.
		// Post data contains:
		// Title
		// Url?
		// ContentMetadata
		// Group
		// |- Page
		// |- Tags
		// Time
		// Author



		return res;
	}
	async reload() {
		return await this.load();
	}
}

module.exports = Profile;
