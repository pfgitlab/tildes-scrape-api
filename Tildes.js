/*global Promise*/

const request = require("request");

function asyncify(fn) {
	return new Promise((resolve, reject) => {
		fn((err, ...res) => {
			if(err) {reject(err); }
			else    {resolve(res);}
		});
	});
}

class Tildes {
	constructor(session) {
		this.session = session;
		this.jar = request.jar();
		this.jar.setCookie(request.cookie(`session=${this.session}`), "https://tildes.net");
		this.headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.68 Safari/534.24"};
	}

	async get(url) {
		let [data] = await asyncify(_ => request({url: url, jar: this.jar, headers: this.headers}, _));
		this.headers = Object.assign(this.headers, data.headers);
		return data;
	}

	// TODO support ajax "callable links"

	getCallableLink(url) {
		return async() => {
			return await this.get(url);
		};
	}

	async post(url, formData) {
		let [data] = await asyncify(_ => request({url: url, jar: this.jar, headers: this.headers, formData: formData}, _));
		this.headers = Object.assign(this.headers, data.headers);
		return data;
	}
}

module.exports = Tildes;
