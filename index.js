const Tildes = require("./Tildes");
const Page = require("./Page");
const Comments = require("./Comments");

const fs = require("fs");

module.exports = {
	Page: require("./Page"),
	Comments: require("./Comments"),
	Profile: require("./Profile"),
};
