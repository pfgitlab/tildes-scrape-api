const cheerio = require("cheerio");

let order = ["activity", "newest", "comments", "votes"];
let period = ["1h", "12h", "24h", "3d", "all", "xh/xd"]; // xh || xd || all

class Page {
	constructor(tildes, group, sorting = {}) { // TODO add an option ot init from a link
		this.tildes = tildes;
		this.group = group;
		this.sorting = sorting;

		this.posts = [];
	}
	sort(orderperiod) {
		this.sorting = Object.assign(this.sorting, orderperiod);
	}
	get url() {
		return `https://tildes.net/${
			encodeURIComponent(this.group)
		}?${
			Object.keys(this.sorting).map(key =>
				`${encodeURIComponent(key)}=${encodeURIComponent(this.sorting[key])}`
			).join`&`
		}`;
	}
	async load(fake) {
		let url = this.url;
		let data = fake ? {body: fake} : await this.tildes.get(this.url);
		// Now for the scraping
		const $ = cheerio.load(data.body);
		// Loop over each article. Each article contains a post
		// Articles contain a header title and a .topic-metadata group and a details description
		let res = [];
		$("article").each((i, jqueryisbad) => {
			let article = $(jqueryisbad);
			
			let data = {};

			let header = article.find("header");
			let titlelink = header.find("h1").find("a");

			let group = article.find(".link-group");

			let textExcerpt = article.find(".topic-text-excerpt");

			if(textExcerpt.length) {
				let summary = textExcerpt.find("summary").find("span");
				let full = textExcerpt.find("p");

				data.content = {
					summary: summary.text(),
					full: full.text()
				};
			}

			let tags = [];

			article.find(".label-topic-tag").each((i, jqib) => {
				let tag = $(jqib);
				let link = tag.find("a");
				tags.push({
					tag: link.text(),
					page: new Page(this.tildes, "", {tag: link.text()})
				});
			});

			// TODO comments = xyz comments, new Comments

			// TODO user pages = author, new userpage()

			let time = article.find(".time-responsive");

			data.time = {
				staticDescription: time.find(".time-responsive-full").text(),
				time: new Date(time.attr("datetime"))
			};

			data.votes = {
				votes: article.find(".topic-voting-votes").text(),
				dataICPutTo: article.find(".topic-voting").attr("data-ic-put-to")
			};

			data.title = titlelink.text();
			data.url = titlelink.attr("href");
			data.contentMetadata = header.find(".topic-content-metadata").text();
			data.group = { // TODO a "group" is called a "group" on the website
				group: group.text(),
				page: new Page(this.tildes, group.text()),
				tags: tags
			};

			res.push(data);
		});
		this.posts = res;
		return res;
	}
	async reload() {
		return await this.load();
	}
}

module.exports = Page;
